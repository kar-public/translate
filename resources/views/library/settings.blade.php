<div class="card mb-3">
    <div class="card-header">Settings</div>
    <div class="card-body">
        {!!Form::open()->put()->route('library.store')!!}
        {!!Form::select('lang_from', 'From', \App\Models\Language::getAllLanguagesList())!!}
        {!!Form::select('lang_to', 'To', \App\Models\Language::getAllLanguagesList())!!}
        {!!Form::submit("Save settings")!!}
        {!!Form::close()!!}
    </div>
</div>