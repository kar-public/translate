<div class="card">
    <div class="card-header">Words list</div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">en</th>
            <th scope="col">ru</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($words as $en => $ru)
            <tr>
                <th scope="row"></th>
                <td>{{$en}}</td>
                <td>{{$ru}}</td>
                <td>TODO</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
