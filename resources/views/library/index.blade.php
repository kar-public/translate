@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-9 col-md-12">
            @include('library.table')
        </div>
        <div class="col-lg-3 col-md-12">
            @include('library.word')
            @include('library.settings')
        </div>
    </div>
</div>
@endsection
