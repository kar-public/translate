<div class="card mb-3">
    <div class="card-header">Add new word</div>
    <div class="card-body">
        {!!Form::open()->put()->route('library.store')!!}
        {!!Form::select('lang', 'Language', \App\Models\Language::getAllLanguagesList())!!}
        {!!Form::text('word', 'Word')!!}
        {!!Form::submit("Add word")!!}
        {!!Form::close()!!}
    </div>
</div>