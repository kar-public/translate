export default function (Vue) {
    let authenticatedUser;
    Vue.auth = {
        passport: {
            client_id: 4,
            client_secet: 'Q8D6O7twZz2nR8SOkghJE7WZCyIgnoY2Sf582y72',
            grant_type: 'password'
        },
        getCsrfToken() {
            return document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
        setToken(token, expiration) {
            localStorage.setItem('token', token);
            localStorage.setItem('expiration', expiration);
        },
        getToken() {
            var token = localStorage.getItem('token');
            var expiration = localStorage.getItem('expiration');

            if (!token || !expiration)
                return null;

            if (Date.now() > expiration) {
                this.destroy();
                return null
            } else {
                return token;
            }
        },
        destroy() {
            localStorage.removeItem('token');
            localStorage.removeItem('expiration');
        },
        isAuthenticated() {
            return !!(this.getToken());
        },
        getAuthenticatedUser() {
            return authenticatedUser;
        },
        setAuthenticatedUser(data) {
            authenticatedUser = data;
        }
    }

    Object.defineProperties(Vue.prototype, {
        $auth: {
            get: () => {
                return Vue.auth
            }
        }
    })
}