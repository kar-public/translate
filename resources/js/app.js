import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import Router from './routes.js'

import VueResource from 'vue-resource'

import Auth from './packages/auth/Auth.js'

Vue.use(Vuex);
Vue.use(VueResource);
Vue.use(Auth);

if (Vue.auth.getToken()) {
    Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken();
}

const store = new Vuex.Store({
    state: {
        settings: {
            translate: 'en-ru',
        },
        from: 'en',
        to: 'ru',
        countries: {
            'en': 'English',
            'ru': 'Russian',
        },
        words: [],
        yandexHost: 'https://translate.yandex.net/api/v1.5/tr.json',
        yandexKey: 'trnsl.1.1.20190402T200159Z.3b0ba3b75b884b2f.5ec95202a9ee5b7f6e2d138baf40243fa4cc1a6e',
    },
    getters: {
        settings: state => state.settings,
    },
    mutations: {
        setCountries(state, countries) {
            state.countries = countries;
        },
        setWords(state, words) {
            state.words = words;
        },
        addWord(state, word) {
            state.words.push(word);
        },
        setSettings(state, settings) {
            state.settings = settings;
            let s = state.settings.translate.split('-');
            state.from = s[0];
            state.to = s[1];
        }
    },
    actions: {}
});

Router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.auth.isAuthenticated()) {
                next({
                    path: '/home'
                })
            } else next()
        }

        if (to.matched.some(record => record.meta.forAuth)) {
            if (!Vue.auth.isAuthenticated()) {
                next({
                    path: '/login'
                })
            } else next()
        } else next()
    }
)

new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router: Router
})
