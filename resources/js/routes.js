import Vue from 'vue';
import VueRouter from 'vue-router';

import Login from './components/autentication/Login.vue';
import Logout from './components/autentication/Logout.vue';
import Register from './components/autentication/Register.vue';
import Home from './components/pages/Home.vue';

Vue.use(VueRouter);

export default new VueRouter({
        routes: [
            {
                path: '/login',
                component: Login,
                meta: {
                    forVisitors: true
                }
            },
            {
                path: '/logout',
                component: Logout,
                meta: {
                    forVisitors: true
                }
            },
            {
                path: '/register',
                component: Register,
                meta: {
                    forVisitors: true
                }
            },
            {
                path: '/',
                component: Home,
                meta: {
                    forAuth: true
                }
            }
        ],
        mode: 'history'
    }
);
