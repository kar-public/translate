<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/test', function () {
        // TODO
        return response()->json([]);
    });
    Route::post('/settings', 'ApiController@getSettings')->name('settings');
    Route::get('/words', 'ApiController@getWords')->name('words');
    Route::post('/settings/save', 'ApiController@settingsSave')->name('settings-save');
    Route::post('/add-word', 'ApiController@addWord')->name('add-word');
});
