<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Services\TranslateService;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ApiController extends Controller
{
    protected $translateService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->translateService = new TranslateService();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 10);
        $words = $this->translateService->getWordsList($offset, $limit);
        return View::make('library.index')->with('words', $words);
    }

    public function getWords(Request $request)
    {
        $user = Auth::user();
//        $offset = $request->get('offset', 0);
//        $limit = $request->get('limit', 10);
        $language = $request->get('language', 'en');
        $words = DB::table('words')
            ->where('language', $language)
            ->where('user_id', $user->id)
//            ->offset($offset)
//            ->limit($limit)
            ->get('word');

        return response()->json(['success' => true, 'data' => $words->toArray()]);
    }

    public function addWord(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'word' => 'required|max:255',
                'language' => 'required|string|max:2',
            ]);
            if ($validator->fails()) {
                // TODO
            }
            $word = strtolower(trim($request->get('word')));
            $language = $request->get('language');
            $user = Auth::user();
            Word::firstOrCreate(
                ['word' => $word, 'language' => $language, 'user_id' => $user->id]
            );
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()]);
        }
    }

    public function getSettings()
    {
        $user = Auth::user();
        $settings = $select = DB::table('settings')
            ->select('name', 'value')
            ->where('user_id', $user->id)
            ->get();
        $data = [];

        foreach ($settings->toArray() as $item) {
            $data[$item->name] = $item->value;
        }
        return response()->json(['data' => $data, 'success' => true]);
    }

    public function settingsSave(Request $request)
    {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'settings' => 'required|array'
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()]);
            }

            foreach ($request->get('settings') as $key => $value) {
                $setting = Setting::firstOrNew(
                    ['name' => $key, 'user_id' => $user->id]
                );
                $setting->fill(['value' => $value]);
                $setting->save();
            }

            return response()->json(['success' => true]);
        } catch (\Throwable $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()]);
        }
    }
}
