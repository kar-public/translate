<?php

namespace App\Http\Controllers;

use App\Services\TranslateService;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class LibraryController extends Controller
{
    protected $translateService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->translateService = new TranslateService();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 10);
        $words = $this->translateService->getWordsList($offset, $limit);
        return View::make('library.index')->with('words', $words);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'word' => 'required|max:255',
            'lang' => 'required|integer|max:10',
        ]);
        if ($validator->fails()) {
            return redirect('library')->withErrors($validator)->withInput();
        }
        try {
            $word = new Word();
            $user = Auth::user();
            $word->user_id = $user->id;
            $word->lang_id = $request->get('lang');
            $word->word = $request->get('word');
            $word->save();
            return redirect()->route('library')->withErrors($validator)->withInput();
        } catch (\Exception $e) {
            $validator->getMessageBag()->add('word', 'Word already exist.');
            return redirect('library')->withErrors($validator)->withInput();
        }
    }
}
