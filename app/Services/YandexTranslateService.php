<?php

namespace App\Services;


class YandexTranslateService
{
    protected $apiKey;
    const HOST = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    public function __construct()
    {
        $this->apiKey = env('YANDEX_TRANSLATE_API_KEY');
    }

    public function translate(array $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->getUrl());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            $this->getPostfields($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $jsonResponse = curl_exec ($ch);

        curl_close ($ch);

        // TODO errors
        $response = json_decode($jsonResponse, true);

        $result = [];
        foreach ($data as $index => $item) {
            $result[$item] = $response['text'][$index];
        }
        return $result;
    }

    private function getUrl()
    {
        // TODO hardcoded
        return self::HOST . '?lang=en-ru&key=' . $this->apiKey;
    }

    private function getPostfields($list)
    {
        $data = [];
        foreach ($list as $item) {
            $data[] = 'text=' . $item;
        }
        return implode('&', $data);
    }

}