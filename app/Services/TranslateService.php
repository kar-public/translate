<?php

namespace App\Services;


use Illuminate\Support\Facades\DB;

class TranslateService
{
    public static function getWordsList($offset = 0, $limit = 10)
    {
        $words = DB::table('words')->skip($offset)->take($limit)->get();
        $translatedData = [];
        foreach ($words as $item) {
            $translatedData[] = $item->word;
        }

        $y = new YandexTranslateService();
        return $y->translate($translatedData);
    }
}
